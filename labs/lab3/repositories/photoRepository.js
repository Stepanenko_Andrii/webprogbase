const Photo = require('../models/photo');
const JsonStorage = require('../jsonStorage');

class PhotoRepository {

    constructor(filePath) {

        this.storage = new JsonStorage(filePath);

    }

    addPhoto(photoModel) {

        const photos = this.storage.readItems();
        this.storage.incrementNextId();
        const newPhoto = new Photo(this.storage.readItems().nextId, photoModel.photoName, photoModel.location, parseInt(photoModel.likes), parseInt(photoModel.dislikes), photoModel.photoDate, photoModel.photoUrl);
        photos.items.push(newPhoto);
        console.log("Photo with id = " + this.storage.readItems().nextId + " has been added successfully.");
        this.storage.writeItems(photos);

        // return newPhoto;
        return this.storage.readItems().nextId;

    }

    getPhotos() {

        return this.storage.readItems().items;

    }

    getPagesNumber(page, per_page, name) {

        const page_size = 3;
        const maxPageSize = 3;

        if (per_page) {

            if (per_page > maxPageSize) {

                console.log("Error.");
    
                return undefined;
    
            }

        }
        else {

            per_page = page_size;

        }

        if (!page) {

            page = 1;

        }

        const photos = this.getPhotos();
        const photosNumber = Number(photos.length)
        const offset = per_page * (page - 1);

        if (photosNumber <= offset) {

            console.log("Error.");

            return undefined;

        }

        let resPhotos = [];
        let tempPhotosLen = 0;

        if (name) {

            for (let i = 0; i < photos.length; i++) {

                if (photos[i].photoName.includes(name)) {
    
                    resPhotos.push(photos[i]);
    
                }
    
            }

            tempPhotosLen = resPhotos.length;
            resPhotos = resPhotos.slice(offset, offset + per_page);

        }

        const currentPhotos = photos.slice(offset, offset + per_page);
        let pagesNumber = 0;

        if ((photosNumber / per_page) - Math.trunc(photosNumber / per_page) != 0) {

            pagesNumber = Math.trunc(photosNumber / per_page) + 1;

        }
        else {

            pagesNumber = Math.trunc(photosNumber / per_page);

        }

        if (name) {

            if ((tempPhotosLen / per_page) - Math.trunc(tempPhotosLen / per_page) != 0) {

                pagesNumber = Math.trunc(tempPhotosLen / per_page) + 1;
    
            }
            else {
    
                pagesNumber = Math.trunc(tempPhotosLen / per_page);
    
            }

            if (pagesNumber == 0) {

                pagesNumber = 1;

            }

            return pagesNumber;

        }

        if (pagesNumber == 0) {

            pagesNumber = 1;

        }
        
        return pagesNumber;

    }

    getPhotosPaginated(page, per_page, name) {

        const page_size = 3;
        const maxPageSize = 3;

        if (per_page) {

            if (per_page > maxPageSize) {

                console.log("Error.");
    
                return undefined;
    
            }

        }
        else {

            per_page = page_size;

        }

        if (!page) {

            page = 1;

        }

        const photos = this.getPhotos();
        const photosNumber = Number(photos.length);
        const offset = per_page * (page - 1);

        if (photosNumber <= offset) {

            console.log("Error.");

            return undefined;

        }

        let resPhotos = [];

        if (name) {

            for (let i = 0; i < photos.length; i++) {

                if (photos[i].photoName.includes(name)) {
    
                    resPhotos.push(photos[i]);
    
                }
    
            }

            resPhotos = resPhotos.slice(offset, offset + per_page);

        }

        const currentPhotos = photos.slice(offset, offset + per_page);

        if (name) {

            return resPhotos;

        }
        
        return currentPhotos;

    }

    getPhotoById(photoId) {

        const photos = this.storage.readItems().items;

        if (photoId < this.storage.readItems().nextId) {

            for (const item of photos) {

                if (item.id === photoId) {

                    return new Photo(item.id, item.photoName, item.location, item.likes, item.dislikes, item.photoDate, item.photoUrl);

                }

            }

            return 0;

        }

        return 0;

    }

    updatePhoto(photoModel) {

        const photos = this.storage.readItems();

        if (photoModel.id < this.storage.readItems().nextId) {

            let check = false;
            let index = 0;

            console.log(photoModel.id);

            for (const item of photos.items) {

                if (parseInt(item.id) === parseInt(photoModel.id)) {

                    photoModel.id = parseInt(photoModel.id);
                    photoModel.likes = parseInt(photoModel.likes);
                    photoModel.dislikes = parseInt(photoModel.dislikes);

                    console.log("Photo has been updated successfully.");
                    check = true;
                    const updatedPhoto = new Photo(photoModel.id, photoModel.photoName, photoModel.location, photoModel.likes, photoModel.dislikes, photoModel.photoDate);
                    photos.items[index] = updatedPhoto;
                    break;

                }

                index++;
                console.log(index);

            }

            if (check === false) {

                console.log("Photo with such id doesn't exist.");

            }

        }
        else {

            console.log("Photo with such id doesn't exist.");

        }

        this.storage.writeItems(photos);

        return photoModel;

    }

    deletePhoto(photoId) {

        let result;
        const photos = this.storage.readItems();

        if (photoId < this.storage.readItems().nextId) {

            let check = false;
            let index = 0;

            for (const item of photos.items) {

                if (parseInt(item.id) === parseInt(photoId)) {

                    check = true;
                    result = photos.items[index];
                    photos.items.splice(index, 1);
                    console.log("Photo has been deleted successfully.");
                    break;

                }

                index++;

            }

            if (check === false) {

                console.log("Photo with such id doesn't exist1.");

            }

        }
        else {

            console.log("Photo with such id doesn't exist.");

        }

        this.storage.writeItems(photos);

        return result;

    }

};

module.exports = PhotoRepository;