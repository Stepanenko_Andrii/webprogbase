class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled, bio) {
        
        this.id = id;
        this.login = login;
        this.fullname = fullname;
        this.role = role;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isEnabled = isEnabled;
        this.bio = bio;

    }
    
 };
 
 module.exports = User;