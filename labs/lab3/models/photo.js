class Photo {

    constructor(id, photoName, location, likes, dislikes, photoDate, photoUrl) {

        this.id = id;
        this.photoName = photoName;
        this.location = location;
        this.likes = likes;
        this.dislikes = dislikes;
        this.photoDate = photoDate;
        this.photoUrl = photoUrl;

    }
    
};

module.exports = Photo;