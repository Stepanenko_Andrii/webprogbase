const router = require('express').Router();
const userController = require('./../controllers/userController');

/**
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */
router.get("/:id", userController.getUserById)

/**
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<User>} User - a page with users
 */
router.get("/", userController.getUsersPaginated)

module.exports = router;
