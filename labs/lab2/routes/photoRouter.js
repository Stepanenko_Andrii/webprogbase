const router = require('express').Router();
const photoController = require('./../controllers/photoController');

// router.get("/", photoController.getPhotos)

/**
 * @route GET /api/photos/{id}
 * @group Photos - photo operations
 * @param {integer} id.path.required - id of the Photo - eg: 1
 * @returns {Photo.model} 200 - Photo object
 * @returns {Error} 404 - Photo not found
 */
router.get("/:id", photoController.getPhotoById)

/**
 * @route POST /api/photos
 * @group Photos - photo operations
 * @param {Photo.model} id.body.required - new Photo object
 * @returns {Photo.model} 201 - added Photo object
 * @returns {Error} 400 - bad request
 */
router.post("/", photoController.addPhoto)

/**
 * @route PUT /api/photos
 * @group Photos - photo operations
 * @param {Photo.model} id.body.required - new Photo object
 * @returns {Photo.model} 200 - changed Photo object
 */
router.put("/", photoController.updatePhoto)

/**
 * @route DELETE /api/photos/{id}
 * @group Photos - uphoto operations
 * @param {integer} id.path.required - id of the Photo - eg: 1
 * @returns {Photo.model} 200 - deleted Photo object
 * @returns {Error} 404 - Photo not found
 */
router.delete("/:id", photoController.deletePhoto)

/**
 * @route GET /api/photos
 * @group Photos - photo operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Photo>} Photo - a page with photos
 */
router.get("/", photoController.getPhotosPaginated)

module.exports = router;