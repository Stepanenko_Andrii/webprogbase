const router = require('express').Router();
const bodyParser = require('body-parser');
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))
const userRouter = require('./userRouter');
router.use('/users', userRouter);
const photoRouter = require('./photoRouter');
router.use('/photos', photoRouter);
const mediaRouter = require('./mediaRouter');
router.use('/media', mediaRouter);

module.exports = router;