const path = require('path');
const PhotoRepository = require('./../repositories/photoRepository');
const { Recoverable } = require('repl');
const photoRepository = new PhotoRepository(path.resolve(__dirname, '../data/photos.json'));

module.exports = {

    async getPhotoById(req, res) {

        try {

            photo = photoRepository.getPhotoById(parseInt(req.params.id));

            if (photo) {

                console.log(photo);
                res.status(200).send({photo: photo, message: "Success."});

            }
            else {

                res.status(404).send({photo: null, message: "Not found."});

            }

        } catch (err) {
            
            console.log(err.message);
            res.status(500).send({photo: null, message: 'Server error.'});

        }
        
    },

    async addPhoto(req, res) {

        try {

            if (!req.body.photoName || !req.body.likes || !req.body.dislikes) {

                res.status(400).send({message: 'Bad request.'});

            }
            else{

                photo = photoRepository.addPhoto(req.body);

                if (photo) {

                    console.log(photo);
                    res.status(201).send({photo: photo, message: "Success."});

                }
                else {

                    res.status(404).send({photo: null, message: "Not found."});

                }

            }
            
        } catch (err) {
           
            console.log(err.message);
            res.status(500).send({photo: null, message: 'Server error.'});

        }
        
    },

    async updatePhoto(req, res) {

        try {

            if (!req.body.photoName || !req.body.likes || !req.body.dislikes) {

                res.status(400).send({message: 'Bad request.'});

            }
            
            photo = photoRepository.updatePhoto(req.body);

            if (photo) {

                console.log(photo);
                res.status(200).send({photo: photo, message: "Success."});

            }
            else {

                res.status(404).send({photo: null, message: "Not found."});

            }

        } catch (err) {
            
            console.log(err.message);
            res.status(500).send({photo: null, message: 'Server error.'});

        }

        
    },

    async deletePhoto(req, res) {

        try {
            
            photo = photoRepository.deletePhoto(req.params.id);

            if (photo) {

                console.log(photo);
                res.status(200).send({photo: photo, message: "Success."});

            }
            else {

                res.status(404).send({photo: null, message: "Not found."});

            }

        } catch (error) {
            
            console.log(err.message);
            res.status(500).send({photo: null, message: 'Server error.'});

        }
        
    },

    async getPhotosPaginated(req, res) {

        try {
            
            photos = photoRepository.getPhotosPaginated(Number(req.query.page), Number(req.query.per_page));

            if (photos) {

                res.status(200).send({photos: photos, message: "Success."});

            }
            else {

                res.status(404).send({photos: null, message: "Not found."});

            }

        } catch (err) {
            
            console.log(err.message);
            res.status(500).send({photos: null, message: 'Server error.'});

        }

    },

};