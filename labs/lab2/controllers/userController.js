const path = require('path');
const UserRepository = require('./../repositories/userRepository');
const userRepository = new UserRepository(path.resolve(__dirname, '../data/users.json'));
const User = require('./../models/user');

module.exports = {

    async getUserById(req, res) {

        try {
            
            user = userRepository.getUserById(parseInt(req.params.id));

            if (user) {

                console.log(user);
                res.status(200).send({user: user, message: "Success."});

            }
            else {

                res.status(404).send({user: null, message: "Not found."});

            }

        } catch (err) {
            
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error.'});

        }
        
    },

    async getUsersPaginated(req, res) {

        try {

            users = userRepository.getUsersPaginated(Number(req.query.page), Number(req.query.per_page));
            
            if (users) {

                console.log(users);
                res.status(200).send({users: users, message: "Success."});

            }
            else {

                res.status(404).send({users: null, message: "Not found."});

            }

        } catch (err) {
            
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error.'});

        }
        
    },

};
