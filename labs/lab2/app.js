const express = require('express');
const app = express();

const apiRouter = require('./routes/apiRouter');

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
 
const options = {
    swaggerDefinition: {
        info: {
            description: 'Trying to understand swagger',
            title: 'Pseudo swagger',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);

app.use('/api', apiRouter);
app.use((req, res) => {
    res.status(400).send({ message: "Error in route."});
});

app.listen(3000, function() {
    console.log('Server is ready');
});