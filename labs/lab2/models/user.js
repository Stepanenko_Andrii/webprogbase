/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname - user's name
 * @property {integer} role.required - 0 if common user, 1 if admin
 * @property {string} registeredAt - date of user registration (ISO 8601)
 * @property {string} avaUrl - url to user's ava
 * @property {bool} isEnabled.required - if the user was activated/deactivated
 */

class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        
        this.id = id;
        this.login = login;
        this.fullname = fullname;
        this.role = role;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isEnabled = isEnabled;

    }
    
 };
 
 module.exports = User;