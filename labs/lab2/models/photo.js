/**
 * @typedef Photo
 * @property {integer} id
 * @property {string} photoName.required - photo name
 * @property {string} location - location, where photo has been taken 
 * @property {integer} likes.required - number of the photo likes
 * @property {integer} dislikes.required - number of the photo dislikes
 * @property {string} photoDate - date, when photo has been taken (ISO 8601)
 */

class Photo {

    constructor(id, photoName, location, likes, dislikes, photoDate) {

        this.id = id;
        this.photoName = photoName;
        this.location = location;
        this.likes = likes;
        this.dislikes = dislikes;
        this.photoDate = photoDate;

    }
    
};

module.exports = Photo;