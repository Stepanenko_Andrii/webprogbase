const Photo = require('../models/photo');
const JsonStorage = require('../jsonStorage');

class PhotoRepository {

    constructor(filePath) {

        this.storage = new JsonStorage(filePath);

    }

    addPhoto(photoModel) {

        const photos = this.storage.readItems();
        this.storage.incrementNextId();
        const newPhoto = new Photo(this.storage.readItems().nextId, photoModel.photoName, photoModel.location, photoModel.likes, photoModel.dislikes, photoModel.photoDate);
        photos.items.push(newPhoto);
        console.log("Photo with id = " + this.storage.readItems().nextId + " has been added successfully.");
        this.storage.writeItems(photos);

        return this.storage.readItems().nextId;

    }

    getPhotos() {

        const photos = this.storage.readItems().items;
        console.log("-------------------------------");

        for (const item of photos) {

            console.log(item.id + ".", "photoName: " + item.photoName.green, "location: " + item.location.blue);

        }

        console.log("-------------------------------");

    }

    getPhotoById(photoId) {

        const photos = this.storage.readItems().items;

        if (photoId < this.storage.readItems().nextId) {

            for (const item of photos) {

                if (item.id === photoId) {

                    return new Photo(item.id, item.photoName, item.location, item.likes, item.dislikes, item.photoDate);

                }

            }

            return 0;

        }

        return 0;

    }

    updatePhoto(photoModel) {

        const photos = this.storage.readItems();

        if (photoModel.id < this.storage.readItems().nextId) {

            let check = false;
            let index = 0;

            for (const item of photos.items) {

                if (item.id === photoModel.id) {

                    console.log("Photo has been updated successfully.");
                    check = true;
                    photos.items[index] = photoModel;
                    break;

                }

                index++;

            }

            if (check === false) {

                console.log("Photo with such id doesn't exist.");

            }

        }
        else {

            console.log("Photo with such id doesn't exist.");

        }

        this.storage.writeItems(photos);

    }

    deletePhoto(photoId) {

        const photos = this.storage.readItems();

        if (photoId < this.storage.readItems().nextId) {

            let check = false;
            let index = 0;

            for (const item of photos.items) {

                if (item.id === photoId) {

                    check = true;
                    photos.items.splice(index, 1);
                    console.log("Photo has been deleted successfully.");
                    break;

                }

                index++;

            }

            if (check === false) {

                console.log("Photo with such id doesn't exist.");

            }

        }
        else {

            console.log("Photo with such id doesn't exist.");

        }

        this.storage.writeItems(photos);

    }

};

module.exports = PhotoRepository;