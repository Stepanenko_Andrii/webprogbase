const User = require('../models/user');
const JsonStorage = require('../jsonStorage');

class UserRepository {

    constructor(filePath) {

        this.storage = new JsonStorage(filePath);

    }

    getUsers() {

        const allUsers = this.storage.readItems().items;
        console.log("-------------------------------");

        for (const item of allUsers) {

            console.log(item.id + ".", "login: " + item.login.green, "fullname: " + item.fullname.blue);

        }

        console.log("-------------------------------");

    }

    getUserById(userId) {

        const items = this.storage.readItems().items;

        for (const item of items) {

            if (item.id === userId) {

                return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);

            }

        }

        return 0;

    }

    addUser(userModel) {

        const items = this.storage.readItems();
        this.storage.incrementNextId();
        const newUser = new User(userModel.id, userModel.login, userModel.fullname, userModel.role, userModel.registeredAt, userModel.avaUrl, userModel.isEnablede);
        items.items.push(newUser);
        this.storage.writeItems(items);

        return this.storage.readItems().nextId;

    }

    updateUser(userModel) {

        const items = this.storage.readItems();
        let check = false;

        for (const item of items.items) {

            if (item.id === userModel.id) {

                check = true;
                items.items[item.id - 1] = userModel;
                break;

            }

        }

        if (check === false) {

            console.log("User with such id doesn't exist.");

        }

        this.storage.writeItems(items);

    }

    deleteUser(userId) {

        const items = this.storage.readItems();
        let check = false;

        for (const item of items.items) {

            if (item.id === userId) {

                check = true;
                items.splice(item.id - 1, 1);
                break;

            }

        }

        if (check === false) {

            console.log("User with such id doesn't exist.");

        }

        this.storage.writeItems(items);

    }
};

module.exports = UserRepository;
