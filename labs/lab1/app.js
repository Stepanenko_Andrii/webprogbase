require('colors');
const readlineSync = require('readline-sync');
const UserRepository = require('./repositories/userRepository');
const userRepository = new UserRepository("data/users.json");
const PhotoRepository = require('./repositories/photoRepository');
const photoRepository = new PhotoRepository("data/photos.json");
const Photo = require('./models/photo');

let ending = false;

while (ending !== true) {

    console.log("All possible commands: ");
    console.log("*".red + " 'get/users' ".green + "to display all users;\n" + "*".red + " 'get/users/{number}' ".green + ", where number == user id, to display some information about specific user;\n" + "*".red + " 'get/photos'".green + " to display all photos;\n" + "*".red + " 'get/photos/{number}'".green + ", where number == photo id, to display specific photo;\n" + "*".red + " 'update/photos/{number}'".green + ", where number == photo id, to update specific photo;\n" + "*".red + " 'delete/photos/{number}'".green + ", where number == photo id, to delete specific photo;\n"+ "*".red + " 'post/photos'".green + " to add new photo");
    const text = readlineSync.question("Enter your command (or exit to end the programm): \n");


    if (text !== "exit") {

        const parts = text.split("/");
        const firstPart = parts[0];
        const secondPart = parts[1];
        const thirdPart = parts[2];

        if (firstPart === "get") {

            if (secondPart === "users") {

                if (thirdPart) {

                    const userById = userRepository.getUserById(parseInt(thirdPart, 10));

                    if (userById) {

                        console.log("-------------------------------");
                        console.log(userById.id.toString().bold + ".", "login: " + userById.login.green, "\nfullname: " + userById.fullname.blue, "\nrole: " + userById.role.toString().yellow, "\nregisteredAt: " + userById.registeredAt.magenta, "\navaUrl: " + userById.avaUrl.red, "\nisEnabled: " + userById.isEnabled.toString().brightBlue);
                        console.log("-------------------------------");

                    }
                    else {

                        console.log("User with such id doesn't exist, please try again.");

                    }

                }
                else {

                    userRepository.getUsers();

                }

            }
            else if (secondPart === "photos") {

                if (thirdPart) {

                    const photoById = photoRepository.getPhotoById(parseInt(thirdPart, 10));

                    if (photoById) {

                        console.log("-------------------------------");
                        console.log(photoById.id.toString().bold + ".", "photoName: " + photoById.photoName.green, "\nlocation: " + photoById.location.blue, "\nlikes: " + photoById.likes.toString().yellow, "\ndislikes: " + photoById.dislikes.toString().magenta, "\nphotoDate: " + photoById.photoDate.red);
                        console.log("-------------------------------");

                    }
                    else {

                        console.log("Photo with such id doesn't exist, please try again.");

                    }

                }
                else {

                    photoRepository.getPhotos();

                }

            }
            else {

                console.log("Incorrect command, please try again.");

            }

        }
        else if (firstPart === "delete" && secondPart === "photos" && thirdPart) {

            photoRepository.deletePhoto(parseInt(thirdPart, 10));

        }
        else if (firstPart === "update" && secondPart === "photos" && thirdPart) {

            console.log("Please, enter new data");
            const photoName = readlineSync.question("new photo name: ");
            const location = readlineSync.question("new location: ");
            const likes = readlineSync.question("new amount of likes: ");
            const dislikes = readlineSync.question("new amount of dislikes: ");
            const photoDate = readlineSync.question("new photo date: ");

            if (isNaN(Date.parse(photoDate))) {

                console.log("Incorrect date input, please try again.");

            }
            else {

                const photo = new Photo(parseInt(thirdPart, 10), photoName, location, parseInt(likes, 10), parseInt(dislikes, 10), photoDate);
                photoRepository.updatePhoto(photo);

            }

        }
        else if (firstPart === "post" && secondPart === "photos") {

            console.log("Please, enter new data");
            const photoName = readlineSync.question("new photo name: ");
            const location = readlineSync.question("new location: ");
            const likes = readlineSync.question("new amount of likes: ");
            const dislikes = readlineSync.question("new amount of dislikes: ");
            const photoDate = readlineSync.question("new photo date: ");
            
            if (isNaN(Date.parse(photoDate)) || isNaN(parseInt(likes, 10)) || isNaN(parseInt(dislikes, 10))) {

                console.log("Incorrect input, please try again.");

            }
            else {

                const photo = new Photo(0, photoName, location, parseInt(likes, 10), parseInt(dislikes, 10), photoDate);
                photoRepository.addPhoto(photo);

            }

        }
        else {

            console.log("Incorrect command, please try again.");

        }

    }
    else {

        ending = true;

    }

}

process.exit(0);