class Photo {

    constructor(id, photoName, location, likes, dislikes, photoDate) {

        this.id = id;
        this.photoName = photoName;
        this.location = location;
        this.likes = likes;
        this.dislikes = dislikes;
        this.photoDate = photoDate;

    }
    
};

module.exports = Photo;