class JsonStorage {
 
    constructor(filePath) {

        this._filePath = filePath;
 
    }
 
    get nextId() {

        return this._parsed_items.nextId;

    }
 
    incrementNextId() {

        this._parsed_items.nextId++;

    }
 
    readItems() {

        const fs = require('fs');
        const buffer = fs.readFileSync(this._filePath);
        const items = buffer.toString();

        try {

            this._parsed_items = JSON.parse(items);

        }
        catch (err) {

            console.log(err.message);

        }

        return this._parsed_items;

    }
 
    writeItems(items) {

        const fs = require('fs');
        const text = JSON.stringify(items, null, 2);

        fs.writeFileSync("./data/photos.json", text, 'utf8');

    }
};
 
module.exports = JsonStorage;